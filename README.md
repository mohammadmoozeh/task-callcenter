# Full-stack JavaScript developer test

### File structure
```
Database.(ts/js)    Simulates our database
app.(ts/js)         Where the magic happens. Put your code in this file
```

Feel free to make new files and install npm packages of your choice. You can use TypeScript or write in plain JavaScript (ES6+).

### Task
We are running a callcenter. This callcenter has queues (telephone lines) to where we receive calls from our customers. 
We also have a number of agents to answer these calls.

Each call has 3 events, that will randomly be simulated by function _CallSimulator_. 
Do not make any changes in this function. Payload of this function is emitted to function _eventHandler_ where you can handle the received payload.
```
RINGING     A calls has been initiated, waiting for an agent to answer.
ANSWER      An agent has answered the call
HANGUP      Call has ended
```


Structure the data according to the clientsides need and send to function _WebsocketEmitter_ **maximum** once per second.
_Do not make these tables. Sending the needed data to function WebsocketEmitter will be enough_

```
ID  Queue name  Calls waiting   Calls ongoing   Calls finished
1   Queue 1     2               1               50
2   Queue 2     0               0               11
...
```

```
ID  Agent name  Status              Calls taken
1   Agent 1     In Call             10
2   Agent 2     Waiting for call    5
...
```

### Think about
- Structure and performance of your code using OOP method
- Amount of data transaction to clientside
- Readability of your code
