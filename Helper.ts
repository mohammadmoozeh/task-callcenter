export let RandomNumber = (end: number, start: number = 0) => {
  return Math.floor(Math.random() * end) + start;
}