"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Agent_1 = require("./Database/Agent");
exports.Agents = Agent_1.default;
var Queue_1 = require("./Database/Queue");
exports.Queues = Queue_1.default;
var AgentQueue_1 = require("./Database/AgentQueue");
exports.AgentQueues = AgentQueue_1.default;
