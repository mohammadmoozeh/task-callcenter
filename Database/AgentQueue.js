"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Agent_1 = require("./Agent");
var Queue_1 = require("./Queue");
var AgentQueues = [];
Agent_1.default.forEach(function (Agent) {
    Queue_1.default.forEach(function (Queue) {
        if (Math.random() > .5)
            AgentQueues.push({
                agent_id: Agent.id,
                queue_id: Queue.id
            });
    });
});
exports.default = AgentQueues;
