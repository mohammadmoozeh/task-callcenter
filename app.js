"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DB = require("./Database");
var Helper_1 = require("./Helper");
var eventHandler = function (payload) {
    console.log(payload);
};
var WebsocketEmitter = console.log;
// DO NOT CHANGE
var CallSimulator = function () {
    var CallID = 0;
    var TOTAL_INDEX = DB.AgentQueues.length;
    setInterval(function () {
        var AgentQueue = DB.AgentQueues[Helper_1.RandomNumber(TOTAL_INDEX)];
        var payload = {
            id: CallID++,
            event: "RINGING",
            queue_id: AgentQueue.queue_id
        };
        eventHandler(payload);
        var TIMEOUT = Helper_1.RandomNumber(5, 1);
        setTimeout(function () {
            payload.event = "ANSWER";
            payload.agent_id = AgentQueue.agent_id;
            eventHandler(payload);
        }, TIMEOUT * 1e3);
        TIMEOUT += Helper_1.RandomNumber(15, 5);
        setTimeout(function () {
            payload.event = "HANGUP";
            payload.agent_id = AgentQueue.agent_id;
            eventHandler(payload);
        }, TIMEOUT * 1e3);
    }, 250);
};
CallSimulator();
